/*
 * referenced:
 * https://subscription.packtpub.com/book/application_development/9781785886195/1/ch01lvl1sec12/switching-between-activities
 * https://developer.android.com/training/basics/firstapp/starting-activity#java
 * https://stackoverflow.com/questions/40584424/simple-android-recyclerview-example
 * https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html
 * https://stackoverflow.com/questions/39439039/how-to-add-overflow-menu-to-toolbar
 * https://stackoverflow.com/questions/2115758/how-do-i-display-an-alert-dialog-on-android
 * https://developer.android.com/reference/android/app/AlertDialog
 */

package edu.sjsu.android.myzoo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CustomAdapter.ItemClickListener{

    CustomAdapter adapter;
    public static final String EXTRA_DESCRIPT = "animalDescriptionParagraph";
    public static final String EXTRA_IMAGE = "animalImage";
    public static final String EXTRA_NAME = "animalName";
    private ArrayList<Animal> animals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // data to populate the RecyclerView with
        animals = new ArrayList<>();
        animals.add(new Animal("Bear", R.drawable.bear,
                "Bears are carnivoran mammals of the family Ursidae. They are classified as caniforms" +
                        ", or doglike carnivorans. Although only eight species of bears are extant, they are widespread, " +
                        "appearing in a wide variety of habitats throughout the Northern Hemisphere and partially in the " +
                        "Southern Hemisphere."));
        animals.add(new Animal("Cat", R.drawable.cat,
                "The cat (Felis catus) is a domestic species of small carnivorous mammal. " +
                        "It is the only domesticated species in the family Felidae and is often referred " +
                        "to as the domestic cat to distinguish it from the wild members of the family. A cat can either be a " +
                        "house cat, a farm cat or a feral cat; the latter ranges freely and avoids human contact."));
        animals.add(new Animal("Cow", R.drawable.cow,
                "Cattle, or cows (female) and bulls (male), are the most common type of large domesticated " +
                        "ungulates. They are a prominent modern member of the subfamily Bovinae, are the most widespread " +
                        "species of the genus Bos, and are most commonly classified collectively as Bos taurus."));
        animals.add(new Animal("Dog", R.drawable.dog,
                "The domestic dog is a domesticated form of wolf. The dog descended from an ancient, extinct wolf, " +
                        "with the modern wolf being the dog's nearest living relative. The dog was the first species " +
                        "to be domesticated by hunter–gatherers more than 15,000 years ago, prior to the development of " +
                        "agriculture."));
        animals.add(new Animal("Whale", R.drawable.whale,
                "Whales are a widely distributed and diverse group of fully aquatic placental marine mammals. " +
                        "They are an informal grouping within the infraorder Cetacea, usually excluding dolphins and " +
                        "porpoises. Whales, dolphins and porpoises belong to the order Cetartiodactyla. Their closest " +
                        "living relatives are the hippopotamuses"));

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rvAnimals);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomAdapter(this, animals);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        //Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
        if(position == adapter.getItemCount()-1)
        {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Are you sure you want to proceed? This is a scary animal");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.cancel();
                    openDetails(position);
                }
            });
            builder1.setNegativeButton("No", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert1 = builder1.create();
            alert1.show();
        }
        else //open normally
        {
            openDetails(position);
        }
    }

    public void openDetails(int position)
    {
        Intent intent = new Intent(this, AnimalDetailsActivity.class);
        String name = adapter.getItem(position);
        intent.putExtra(EXTRA_NAME, name);
        String descript = adapter.getDescription(position);
        intent.putExtra(EXTRA_DESCRIPT, descript);
        int fn = adapter.getImage(position);
        intent.putExtra(EXTRA_IMAGE, fn);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == R.id.miInformation) {
            Intent openInfoIntent = new Intent(this, ZooInformationActivity.class);
            startActivity(openInfoIntent);
            return true;
        }
        else if(id == R.id.miUninstall) {
            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE);
            uninstallIntent.setData(Uri.parse("package:"+ this.getPackageName()));
            startActivity(uninstallIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}