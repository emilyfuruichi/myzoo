package edu.sjsu.android.myzoo;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class ZooInformationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoo_information);
    }

    public void onClickCall(View view)
    {
        String myPhoneNumberUri = "tel:888-8888";
        Intent myCall = new Intent(Intent.ACTION_DIAL, Uri.parse(myPhoneNumberUri));
        startActivity(myCall);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == R.id.miInformation) {
            Intent openInfoIntent = new Intent(this, ZooInformationActivity.class);
            startActivity(openInfoIntent);
            return true;
        }
        else if(id == R.id.miUninstall) {
            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE);
            uninstallIntent.setData(Uri.parse("package:"+ this.getPackageName()));
            startActivity(uninstallIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}