package edu.sjsu.android.myzoo;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_details);

        Intent intent = getIntent();
        String name = intent.getStringExtra(MainActivity.EXTRA_NAME);
        TextView header = findViewById(R.id.animalName);
        header.setText(name);
        String description = intent.getStringExtra(MainActivity.EXTRA_DESCRIPT);
        int image = intent.getIntExtra(MainActivity.EXTRA_IMAGE,0);
        TextView textView = findViewById(R.id.detailsParagraph);
        textView.setText(description);
        ImageView imageView = findViewById(R.id.animalImageView);
        imageView.setImageResource(image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == R.id.miInformation) {
            Intent openInfoIntent = new Intent(this, ZooInformationActivity.class);
            startActivity(openInfoIntent);
            return true;
        }
        else if(id == R.id.miUninstall) {
            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE);
            uninstallIntent.setData(Uri.parse("package:"+ this.getPackageName()));
            startActivity(uninstallIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}