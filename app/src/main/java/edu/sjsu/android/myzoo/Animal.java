package edu.sjsu.android.myzoo;

public class Animal
{
    private String animalName;
    private int image;
    private String someDetail;

    public Animal(String animalName, int image, String someDetail)
    {
        this.animalName = animalName;
        this.image = image;
        this.someDetail = someDetail;
    }

    public String getName()
    {
        return animalName;
    }

    public int getImage() {
        return image;
    }

    public String getDetail()
    {
        return someDetail;
    }
}
